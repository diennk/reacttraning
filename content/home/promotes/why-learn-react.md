---
title: Tại sao nên học React?
order: 0
---

React được phát triển bởi Facebook, cộng đồng React rất mạnh với hàng triệu lập trình viên trên toàn thế giới.
 
React rất dễ học và không yêu cầu người học có kiến thức sâu về lập trình, chỉ cần kiến thức cơ bản về lập trình Web bạn đã sẵn sàng với việc tiếp cận React.



