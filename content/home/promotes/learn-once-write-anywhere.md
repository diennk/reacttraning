---
title: Web, Mobile, Desktop Apps
order: 1
---

Học React bạn có thể làm được gì? Làm được tất cả các loại ứng dụng từ Web app đến Mobile app hay thậm chí là cả Desktop app.

Thật tuyệt vời khi chúng ta chỉ phải học một lần, nhưng lại làm được rất nhiều thứ từ React. Điều đó tiết kiệm cho bạn khá nhiều thời gian và công sức khi xây dựng những ứng dụng trên các nền tảng khác nhau. 
