---
id: khoa-hoc-react-native
title: Khóa học React Native
description: Xây dựng các ứng dụng Android và iOS với React Native. Sau khóa học bạn có đủ kỹ năng để tự xây và quản lý ứng dụng của mình.
days: 12
order: 2
permalink: courses/khoa-hoc-react-native.html
redirect_from:
cover_image: "./react-native.png"
---

Khoá học lập trình App với React Native 
