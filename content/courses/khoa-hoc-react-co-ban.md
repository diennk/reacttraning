---
id: khoa-hoc-react-co-ban
title: Khóa học React cơ bản
description: Khóa học dành cho đối tượng mới bắt đầu với React. Đây là khóa học tốt nhất giúp bạn nhanh chóng nắm được các khái niệm cơ bản của React, từng bước nắm vững công nghệ này.
days: 8
order: 1
permalink: courses/khoa-hoc-react-co-ban.html
next: khoa-hoc-react-nang-cao.html
redirect_from:
cover_image: "./reactjs.png"

---

Nội dung khóa học được **cập nhật** liên tục với phiên bản React mới nhất.

**React** rất mạnh để xây dựng các ứng dụng Web và Mobile, tuy nhiên nếu bạn là người mới bắt đầu làm quen với React hẳn các bạn gặp rất nhiều rắc rối khiến việc học React như một cơn ác mộng. Ngay cả khi bạn có một mã nguồn ứng dụng React tải từ Github bạn cũng không biết cách dựng môi trường đầy đủ để chạy được ứng dụng. Trong khóa học này, chúng ta sẽ bắt đầu từ một thư mục trống và chúng ta sẽ xây dựng một ứng dụng bao gồm mọi thứ bạn cần từng bước làm chủ được React (bao gồm việc chuyển qua lại giữa các màn hình hay việc kết nối với API,...). Khóa học này được học viên đánh giá rất cao phù hợp với người mới bắt đầu làm quen với React, muốn học React một cách có hệ thống, nắm chắc kiến thức cơ bản trước khi nâng lên trình độ cao hơn. Khóa học được cập nhật và tương thích với React 16.7.


## Yêu cầu của khóa học
Có kiến thức cơ bản về web (HTML, CSS, JavaScript) sẽ giúp bạn học React nhanh. Nếu bạn là người mới hoàn toàn với web bạn sẽ gặp đôi chút khó khăn nhưng bạn vẫn có thể học được.

## Bạn sẽ học gì trong khóa học này

- Cài đặt môi trường và công cụ lập trình
- Yarn, NPM
- Javascript
- React 16.7
- Xây dựng ứng dụng todo list(CRUD)
- Build và Deploy ứng dụng lên host


## Những ai phù hợp với khóa học này?

- Các lập trình viên đang làm back-end biết về JavaScript nhưng muốn tìm hiểu về React càng nhanh càng tốt.
- Sinh viên mới tốt nghiệp, những người có kiến thức cơ bản về JavaScript và muốn dễ tìm việc làm hơn bằng cách học React.
- Lập trình viên đang làm với Angular nhưng muốn tìm hiểu về lý do tại sao React trở nên phổ biến hơn.
- Lập trình viên đang làm với jQuery hoặc Backbone muốn nâng cấp khả năng xây dựng UI của ứng dụng.
- Lập trình viên Mobile  Android, iOS muốn tìm hiểu xem khả năng của React có thể làm được gì những với ứng dụng Mobile.
- Các lập trình viên front end muốn hiện đại hóa bộ kỹ năng của họ để được tăng lương hoặc áp dụng cho một công việc khác.


## Đăng ký học 👉 [ tại đây ](/courses/dang-ky-hoc-react.html) ##