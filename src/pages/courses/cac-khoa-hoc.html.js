/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * @emails react-core
 * @flow
 */

import {graphql, Link} from 'gatsby';
import Layout from 'components/Layout';
import Container from 'components/Container';
import Header from 'components/Header';
import TitleAndMetaTags from 'components/TitleAndMetaTags';
import React from 'react';
import {urlRoot} from 'site-constants';
import {colors, media, sharedStyles} from 'theme';
import toCommaSeparatedList from 'utils/toCommaSeparatedList';
import MetaTitle from 'templates/components/MetaTitle';
import Img from 'gatsby-image';

import type {allMarkdownRemarkData} from 'types';

type Props = {
  data: allMarkdownRemarkData,
  location: Location,
};

const AllCourses = ({data, location}: Props) => (
  <Layout location={location}>
    <Container>
      <div css={sharedStyles.articleLayout.container}>
        <div css={sharedStyles.articleLayout.content}>
          <Header>
            <div css={{textAlign: 'center', marginTop : 30}}>
              Đừng lãng phí thời gian của bạn với những tài liệu khó hiểu và lỗi
              thời.
            </div>
          </Header>
          <h2 css={{textAlign: 'center'}}>
            Tất cả các khóa học của chúng tôi được đảm bảo cập nhật với phiên
            bản React mới nhất.
          </h2>
          <TitleAndMetaTags
            ogUrl={`${urlRoot}/blog/all.html`}
            title="Các khóa học React"
          />
          <ul
            css={{
              display: 'flex',
              flexWrap: 'wrap',
            }}>
            {data.allMarkdownRemark.edges.map(({node}) => {
              if (node.fields.slug.indexOf('community') >= 0) return null;
              return (
                <li
                  css={{
                    marginTop: 40,
                    width: '100%',
                    padding: 10,

                    [media.size('medium')]: {
                      width: '50%',
                    },

                    [media.greaterThan('large')]: {
                      width: '50%',
                    },
                  }}
                  key={node.fields.slug}>
                  <div
                    css={{
                      border: '1px solid #dfdfdf',
                      height: '100%',
                    }}>
                    <Link
                      css={{
                        borderBottom: '1px solid #ececec',
                        ':hover': {
                          borderBottomColor: colors.black,
                        },
                      }}
                      key={node.fields.slug}
                      to={node.fields.slug}>
                      {node.frontmatter.cover_image && (
                        <Img
                          css={{height: 300}}
                          sizes={
                            node.frontmatter.cover_image.childImageSharp.sizes
                          }
                        />
                      )}
                    </Link>

                    <div css={{padding: 10}}>
                      <h2
                        css={{
                          fontSize: 18,
                          color: colors.dark,
                          lineHeight: 1.5,
                          fontWeight: 700,
                          textTransform: 'uppercase',
                        }}>
                        {node.frontmatter.title}
                      </h2>
                      {node.frontmatter.days && (
                        <MetaTitle>
                          Thời lượng {node.frontmatter.days} buổi học
                        </MetaTitle>
                      )}
                      <p>{node.frontmatter.description}</p>
                      <MetaTitle>{node.fields.date}</MetaTitle>
                      {node.frontmatter.author ? (
                        <div
                          css={{
                            color: colors.subtle,
                          }}>
                          by{' '}
                          {toCommaSeparatedList(
                            node.frontmatter.author,
                            author => (
                              <span key={author.frontmatter.name}>
                                {author.frontmatter.name}
                              </span>
                            ),
                          )}
                        </div>
                      ) : null}
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </Container>
  </Layout>
);

export const pageQuery = graphql`
  query AllCoursesPageQuery {
    allMarkdownRemark(
      filter: {fileAbsolutePath: {regex: "/courses/"}}
      sort: {fields: [frontmatter___order], order: ASC}
    ) {
      edges {
        node {
          frontmatter {
            title
            description
            days
            order
            author {
              frontmatter {
                name
                url
              }
            }
            cover_image {
              publicURL
              childImageSharp {
                sizes(maxWidth: 1240) {
                  srcSet
                }
              }
            }
          }
          fields {
            date(formatString: "MMMM DD, YYYY")
            slug
          }
        }
      }
    }
  }
`;

export default AllCourses;
