/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * @emails react-core
 * @flow
 */

import {graphql, Link} from 'gatsby';
import Layout from 'components/Layout';
import Container from 'components/Container';
import Header from 'components/Header';
import TitleAndMetaTags from 'components/TitleAndMetaTags';
import React from 'react';
import {urlRoot} from 'site-constants';
import {colors, media, sharedStyles} from 'theme';
import toCommaSeparatedList from 'utils/toCommaSeparatedList';
import MetaTitle from 'templates/components/MetaTitle';

import type {allMarkdownRemarkData} from 'types';

type Props = {
  data: allMarkdownRemarkData,
  location: Location,
};
let styles = {
	input : {border: 'solid 1px #dfdfdf', borderRadius : 5, padding : 10, marginTop : 20, width : '100%', fontSize: 20},
	button : {border: 'solid 1px #dfdfdf', borderRadius : 5, padding : 10, marginTop : 20, width : '100%', fontSize: 20, backgroundColor: 'green', color : 'white'}
}
let model = {}
let onChange = (field, value) => {
	model[field] = value
}
let onSubmit = () => {
	console.log(model)
}
const LearningRegistratrion = ({data, location}: Props) => (
  <Layout location={location}>
    <Container>
      <div>
        <div css={{maxWidth : 500, margin: '30px auto', padding:'0 20px 20px 20px', border: 'solid 1px #dfdfdf', borderRadius : 5}}>
          <h1>
            <div css={{textAlign: 'center'}}>Đăng ký học React.</div>
          </h1>
          <p css={{textAlign: 'center'}}>Bạn có thể để lại các thông tin bên dưới, 
ReactTraining.VN sẽ gọi lại ngay cho bạn để tư vấn miễn phí</p>
          <TitleAndMetaTags
            ogUrl={`${urlRoot}/blog/all.html`}
            title="Đăng ký học React"
          />
		  
		  <input css={styles.input} placeHolder='Họ và tên' onChange={e => onChange('name', e.target.value)}/>
		  <input css={styles.input} placeHolder='Số điện thoại' onChange={e => onChange('phone', e.target.value)}/>
		  <input css={styles.input} placeHolder='Email' onChange={e => onChange('email', e.target.value)}/>
		  <button css={styles.button} onClick={onSubmit}>Đăng ký</button>
		  
		  <p>Nếu bạn cần liên hệ ngay với ReactTraining.VN, vui lòng gọi qua số Hotline:

Hà Nội: 070202.69.86</p>
        </div>
      </div>
    </Container>
  </Layout>
);

export default LearningRegistratrion;
