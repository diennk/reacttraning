/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * @emails react-core
 */

import MarkdownPage from 'components/CustomMarkdownPage';
import PropTypes from 'prop-types';
import React from 'react';
import {graphql} from 'gatsby';
import Layout from 'components/Layout';
import {createLinkCourses} from 'utils/createLink';
import {sectionListCourses} from 'utils/sectionList';

console.log(sectionListCourses);
const Courses = ({data, location}) => (
  <Layout location={location}>
    <MarkdownPage
      createLink={createLinkCourses}
      location={location}
      markdownRemark={data.markdownRemark}
      sectionList={sectionListCourses}
      titlePostfix=" &ndash; React Training Vietnam"
    />
  </Layout>
);

Courses.propTypes = {
  data: PropTypes.object.isRequired,
};

export const pageQuery = graphql`
  query TemplateCoursesMarkdown($slug: String!) {
    markdownRemark(fields: {slug: {eq: $slug}}) {
      html
      frontmatter {
        title
        next
        prev
      }
      fields {
        path
        slug
      }
    }
  }
`;

export default Courses;
