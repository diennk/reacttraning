/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * @emails react-core
 * @flow
 */

import Container from 'components/Container';
import HeaderLink from './HeaderLink';
import {Link} from 'gatsby';
import React from 'react';
import {colors, fonts, media} from 'theme';
import {version} from 'site-constants';
import ExternalLinkSvg from 'templates/components/ExternalLinkSvg';
import DocSearch from './DocSearch';

import logoSvg from 'icons/logo.svg';

const Header = ({location}: {location: Location}) => (
  <header
    css={{
      backgroundColor: colors.darker,
      color: colors.white,
      position: 'fixed',
      zIndex: 1,
      width: '100%',
      top: 0,
      left: 0,
    }}>
    <Container>
      <div
        css={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: 60,
          [media.between('small', 'large')]: {
            height: 50,
          },
          [media.lessThan('small')]: {
            height: 40,
          },
        }}>
        <Link
          css={{
            display: 'flex',
            marginRight: 20,
            height: '100%',
            alignItems: 'center',
            color: colors.brand,

            ':focus': {
              outline: 0,
              color: colors.white,
            },

            [media.greaterThan('small')]: {
              width: 'calc(100% / 6)',
            },
            [media.lessThan('small')]: {
              flex: '0 0 auto',
            },
          }}
          to="/">
          <img src={logoSvg} alt="" height="40" />
          <span
            css={{
              color: 'inherit',
              marginLeft: 10,
              fontWeight: 600,
              fontSize: 20,
              lineHeight: '20px',
              [media.lessThan('large')]: {
                fontSize: 16,
                marginTop: 1,
              },
              [media.lessThan('small')]: {
                // Visually hidden
                position: 'absolute',
                overflow: 'hidden',
                clip: 'rect(0 0 0 0)',
                height: 1,
                width: 1,
                margin: -1,
                padding: 0,
                border: 0,
              },
            }}>
            REACTTRAINING.VN
          </span>
        </Link>

        <nav
          css={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'stretch',
            overflowX: 'auto',
            overflowY: 'hidden',
            WebkitOverflowScrolling: 'touch',
            height: '100%',
            width: '60%',

            [media.size('xsmall')]: {
              flexGrow: '1',
              width: 'auto',
            },
            [media.greaterThan('xlarge')]: {
              width: null,
            },
            [media.lessThan('small')]: {
              maskImage:
                'linear-gradient(to right, transparent, black 20px, black 90%, transparent)',
            },
          }}>
          {/*<HeaderLink
            isActive={location.pathname.includes('/docs/')}
            title="Docs"
            to="/docs/getting-started.html"
          />
          <HeaderLink
            isActive={location.pathname.includes('/tutorial/')}
            title="Tutorial"
            to="/tutorial/tutorial.html"
          />
          <HeaderLink
            isActive={location.pathname.includes('/community/')}
            title="Community"
            to="/community/support.html"
          />
          <HeaderLink
            isActive={location.pathname.includes('/blog')}
            title="Blog"
            to="/blog/"
          /> */}
          <HeaderLink
            isActive={location.pathname.includes('/courses/')}
            title="Khóa học"
            to="/courses/cac-khoa-hoc.html"
          />
          {/* <HeaderLink
            isActive={location.pathname.includes('/tu-van')}
            title="Tư vấn"
            to="/blog/"
          /> */}
          <HeaderLink
            isActive={location.pathname.includes('/blog')}
            title="Bài viết"
            to="/blog/"
          />
          <HeaderLink
            isActive={location.pathname.includes('/lien-he')}
            title="Liên hệ"
            to="/blog/"
          />
        </nav>

        {/* <DocSearch /> */}

        <div
          css={{
            [media.lessThan('medium')]: {
              display: 'none',
            },
            [media.greaterThan('large')]: {
              width: 'calc(100% / 6)',
            },
            textAlign: 'right',
          }}>
          {/* <Link
            css={{
              padding: '5px 10px',
              whiteSpace: 'nowrap',
              ...fonts.small,

              ':hover': {
                color: colors.brand,
              },

              ':focus': {
                outline: 0,
                backgroundColor: colors.lighter,
                borderRadius: 15,
              },
            }}
            to="/versions">
            v{version}
          </Link> */}
          {/* <a
            css={{
              padding: '5px 10px',
              marginLeft: 10,
              whiteSpace: 'nowrap',
              ...fonts.small,

              ':hover': {
                color: colors.brand,
              },

              ':focus': {
                outline: 0,
                backgroundColor: colors.lighter,
                borderRadius: 15,
              },
            }}
            href="https://github.com/facebook/react/"
            target="_blank"
            rel="noopener">
            GitHub
            <ExternalLinkSvg
              cssProps={{
                marginLeft: 5,
                verticalAlign: -2,
                color: colors.subtle,
              }}
            />
          </a> */}
          <a href="tel:+84702026986">📞 0702026986</a>
        </div>
      </div>
    </Container>
  </header>
);

export default Header;
